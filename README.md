# Spring Boot CRUD Rest API for a Person with a search function.


### Description

This API implements possibility to create, update, remove a Person type object 
or to get a Person type object one-by-one or in a bulk mode as a list. 
All created at runtime objects store in a special structure (Repository).
Also, there also implemented searching Person type objects by several fields.

##### Creating/Updating/Removing/Getting of a object
   
If you want to get a initial list of persons, please use `GET` query : 
`/persons`.

To obtain a specific record use `GET` query 
`/person/[id]` where id is a identifier of the record in the repository.

Please note, the identifier `id` should be more than zero `> 0` and the repository should contain a record with specified `id`.
Otherwise, you would receive an appropriate error message and a response status code = `400`.

To create a record use `PUT` query 
`/person`. You are allowed to specify all attributes of the new record except identifier `id` in the request body. 
 
 Example:
```json
{
    "name":       "name",
    "secondName": "secondName",
    "thirdName":  "thirdName",
    "gender":     "WOMAN",
    "birthday":   "2000-01-01",
    "legalType":  "NATURAL"
}
```  

Use the same `PUT` query `/person` for updating an existing record. 
However, in this case it is necessary to specify a identifier of a record being changed. 

Like this:
```json
{
    "id": 1,
    "name":       "name",
    "secondName": "secondName",
    "thirdName":  "thirdName",
    "gender":     "WOMAN",
    "birthday":   "2000-01-01",
    "legalType":  "NATURAL"
}
```
If there is no object with a specified identifier when will be thrown up an error with a response status code = `400`.

Use `DELETE` query `person/[id]` in order to remove an object. There is `id` is an identifier of existing record, 
which will be removed.
If there is no record with specified `id` when you will receive an error message with a response status code = `400`.

##### Search

One of the essential features of the API is a possibility to search records by specified fields of the entity Person,
For the purpose is used an annotation `@Searchable` in class `Person` and an implementation of dictionary in the `DAO`.

It is possible to search by fields `name`, `secondName`, `thirdName`.
Before starting a search, you can query a list of the fields, which are supported by the searching function 
using `GET` query `persons/search/available/fields/[entity-name]`,
where: 
a path variable `entity-name` can contain such values as: `Person`, `Client`, `Subscriber` etc. 

Today, a search implemented only for the entity Person.
To start a search use a `POST` query `/persons/search` with the body:
```json
{
	"fieldName": "secondName",
	"fieldValue": "substring",
	"isExactEquivalence": true
}
```
where 
`fieldName` is a field name, which supports a search, 
`sample` is a required substring, 
`isExactEquivalence` is a flag which indicates a requested search type: a exact match search or a search by substring.

Please note: a search is case-insensitive.


### Clone 
Clone the repo for starting work with API:
```sh
git clone https://PavelPraulov@bitbucket.org/PavelPraulov/person-rest-crud-api-with-search.git
```
### Up & Run
If you use Maven, you can run the application using 
```sh
mvn spring-boot:run
```

Otherwise you can build and run the JAR file:
```sh
mvn clean package
java -jar target/gs-rest-service-0.1.0.jar
```

### API
| Request | Type | Description | 
| ------- | ---- | ------ | 
| `/persons` | `GET` | Get all existing objects of a Person|
| `/person/[id]` | `GET` | Get a person by `id`|
| `/person` | `PUT` |  Update or create a person (depend on a request body)|
| `/person/[id]` | `DELETE` | Delete a person by `id`|
| `persons/search/available/fields/[entity-name]` | `GET` | Get a list of fields which support a search|
| `persons/search` | `POST` | Search in entity fields|

### Test the service
To test that the service is up, visit `http://localhost:8080/greeting`, where you will see:
```json
{"id":1,"content":"Hello, World!"}
```
Just by providing a value for the path variable `name` e.g: `http://localhost:8080/greeting?name=User`, 
you will notice that a value of a response field will be changed from "Hello, World!" to "Hello User!":
```json
{"id":2,"content":"Hello, User!"}
```
