package ru.pd.personsearch.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pd.personsearch.controller.dto.PersonObject;
import ru.pd.personsearch.controller.dto.SearchDescriptorObject;
import ru.pd.personsearch.controller.dto.SearchSupportedObjectList;
import ru.pd.personsearch.dao.PersonDao;
import ru.pd.personsearch.model.search.EntitySearchMetadataProvider;

import java.util.List;

/**
 * Processor for Person API.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.2 17.05.2019
 */
@Component
public class PersonProcessor {

    @Autowired
    private PersonDao personDAO;

    /**
     * A Search of a {@link PersonObject} by field value which is described in the {@code descriptor}.
     *
     * @param  descriptor search parameters
     * @return Person object
     */
    //TODO: A test is needed
    public List<PersonObject> getPersonsByFieldValue(SearchDescriptorObject descriptor) {
        /*
           The place for some useful code,
           working with entities,
           business logic
           ...
         */
        return PersonObject.getPersonObjects(
                personDAO.getPersons(
                        descriptor.getFieldName(),
                        descriptor.getFieldValue(),
                        descriptor.getIsExactEquivalence()));
    }

    /**
     * Return an object with field names which support searching.
     *
     * @param  entityName name of Entity for searching in.
     * @return an object with field names which support searching
     */
    public SearchSupportedObjectList getSearchableFields(String entityName) {
        return new SearchSupportedObjectList(
                EntitySearchMetadataProvider.getClassFieldsInfo(entityName).values());
    }

    /**
     * Delete a person with specified {@code id}.
     *
     * @param id person ID
     */
    public void deletePerson(long id) {
        personDAO.deletePerson(id);
    }


    /**
     * Return a person with specified {@code id}.
     *
     * @param  id person ID
     * @return Person object
     */
    public PersonObject getPerson(long id) {
        return new PersonObject(personDAO.getPerson(id));
    }

    /**
     * Return all persons from a storage.
     *
     * @return a list of Person objects
     */
    public List<PersonObject> getAllPersons() {
        return PersonObject.getPersonObjects(personDAO.getAllPersons());
    }

    /**
     * Update person in a storage using parameter object with changes.
     *
     * @param  modifiedPerson person object with changes
     * @return person object with applied changes
     */
    public PersonObject updatePerson(PersonObject modifiedPerson) {
        return new PersonObject(personDAO.mergePerson(modifiedPerson.parse()));
    }




}
