package ru.pd.personsearch.dao;

import ru.pd.personsearch.model.entity.Person;

/**
 * An entity which is reacted for the special actions
 * in a storage {@link PersonRepository}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
public interface ChangeStorageSetReactable {
    /**
     * React on the action of putting new Person into a storage.
     *
     * @param id   person ID
     * @param item person
     */
    void putItem(long id, Person item);

    /**
     * React on the action of removing a Person from a storage.
     *
     * @param id   person ID
     */
    void removeItem(long id);
}
