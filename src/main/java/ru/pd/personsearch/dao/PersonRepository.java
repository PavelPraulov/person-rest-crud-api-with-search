package ru.pd.personsearch.dao;

import ru.pd.personsearch.model.entity.Person;
import ru.pd.personsearch.model.exception.PersonServiceException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A storage of Person API.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
public class PersonRepository {
    /**
     * A map which collects {@link Person} entries associated with person ID.
     */
    private final Map<Long, Person> persons = new HashMap<>();

    private long personIdSequence = 0L;
    long generateId() {
        if (personIdSequence == 0 && !persons.isEmpty()) {
            personIdSequence = persons.entrySet().stream()
                    .max(Comparator.comparingLong(Map.Entry::getKey))
                    .get().getKey() + 1;
        }
        return personIdSequence++;
    }

    /**
     * Dictionaries of Person field values,
     * which are subscribed on some operations of a storage and
     * will react when these operations will be executed.
     *
     * i.e.: on put a new entry, on delete a entry from a storage.
     */
    private List<ChangeStorageSetReactable> subscribedDictionaries = new ArrayList<>();

    /**
     * Subc
     * @param dictionary
     */
    void subscribeForRepositoryChangeEvent(ChangeStorageSetReactable dictionary) {
        subscribedDictionaries.add(dictionary);
    }

    /**
     * Notify all string dictionaries about
     * happened putting event into common storage.
     *
     * @param id   identifier of the new entry
     * @param item new common storage entry
     */
    private void notifyOnPutEvent(Long id, Person item) {
        subscribedDictionaries.forEach(callable -> callable.putItem(id, item));
    }

    private void notifyOnRemoveEvent(Long id) {
        subscribedDictionaries.forEach(callable -> callable.removeItem(id));
    }

    /**
     * Put the item into the common storage
     * and call a putting event on all
     * subscribed dictionaries.
     *
     * @param  item new person to be stored
     * @return stored item
     */
    Person putPerson(Person item) {
        persons.put(item.getId(), item);
        notifyOnPutEvent(item.getId(), item);
        return item;
    }

    Person getPerson(long id) {
        checkId(id);
        return persons.get(id);
    }

    Person updatePerson(Person item) {
        persons.put(item.getId(), item);
        notifyOnPutEvent(item.getId(), item);
        return item;
    }

    Person mergePerson(Person item) {
        if (item.getId() == null) {
            item.setId(generateId());
        } else {
            checkId(item.getId());
        }
        return updatePerson(item);
    }

    void deletePerson(long id) {
        checkId(id);
        persons.remove(id);
        notifyOnRemoveEvent(id);
    }

    List<Person> getAllPersons() {
        Collection<Person> collection = persons.values();
        return new ArrayList<>(collection);
    }

    /**
     * Check if the storage contains a record with specified id,
     * otherwise, it throws {@link PersonServiceException}.
     *
     * @param  id storage record ID
     * @throws PersonServiceException
     */
    // TODO: need a test
    private void checkId(long id) {
        if (id < 0) {
            throw new PersonServiceException(String.format("You specified id=[%s], but it cannot be negative.", id));
        }
        if (persons.get(id) == null) {
            throw new PersonServiceException(String.format("Person with id=[%s] doesn't exists.", id));
        }
    }
}
