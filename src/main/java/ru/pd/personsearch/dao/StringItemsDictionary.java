package ru.pd.personsearch.dao;

import ru.pd.personsearch.model.entity.Person;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collector;

import static java.util.Comparator.comparing;

/**
 * A sorted dictionary of any string values
 * in a storage {@link PersonRepository}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
public abstract class StringItemsDictionary implements ChangeStorageSetReactable {

    /**
     * A base container for dictionary items
     */
    private Map<Long, String> items = new LinkedHashMap<>();

    protected void sortDictionaryItems() {
        items = items.entrySet().stream()
                .sorted(comparing(Map.Entry<Long, String>::getValue).thenComparing(Map.Entry::getKey))
                .collect(LinkedHashMap::new,(map,entry) -> map.put(entry.getKey(),entry.getValue()),LinkedHashMap::putAll);
    }

    /**
     * Default implementation of putting method
     * for a string dictionary.
     *
     * @param item putting item
     * @param id   id of the putting item
     */
    protected void putItem(String item, long id) {
        items.put(id, item);
        sortDictionaryItems();
    }

    /**
     * Default implementation of deleting method
     * for a string dictionary.
     *
     * @param id id of the deleting item
     */
    protected void deleteItem(long id) {
        items.remove(id) ;
        sortDictionaryItems();
    }

    /**
     * Default implementation of all items getting method
     * for a string dictionary.
     */
    protected List<String> getItems() {
        return new ArrayList<>(items.values());
    }

    /**
     * Retrieve dictionary map entry keys which associated with a value matches a {@code searchValue}.
     *
     * @param  searchValue a value to be searched
     * @param  isExactEquivalence a flag of exact matching
     * @return list of {@link Person} id
     */
    protected List<Long> getKeysByValueEquivalence(String searchValue, Boolean isExactEquivalence) {

        // Describe a method of filtering entries
        Predicate<Map.Entry<Long, String>> predicate = (isExactEquivalence)
                ? entry -> entry.getValue().equals(searchValue)
                : entry -> entry.getValue().toLowerCase().contains(searchValue.toLowerCase());

        Collector<Map.Entry<Long, String>, List<Long>, List<Long>> collector = Collector.of(
                ArrayList::new,
                (hits, entry) -> hits.add(entry.getKey()),
                (hits, output) -> output = hits);

        // Retrieve from the container all entries satisfy the predicate
        return items.entrySet().stream().filter(predicate).collect(collector);
    }

    @Override
    public abstract void putItem(long id, Person item);

    @Override
    public void removeItem(long id) {
        deleteItem(id);
    }
}
