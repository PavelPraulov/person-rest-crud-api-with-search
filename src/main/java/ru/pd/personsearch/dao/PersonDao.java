package ru.pd.personsearch.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.pd.personsearch.controller.dto.PersonObjectStorage;
import ru.pd.personsearch.model.entity.Person;
import ru.pd.personsearch.model.exception.PersonServiceException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;

/**
 * DAO of Person API.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Component
@Repository
public class PersonDao {

    /**
     * A storage for the persons.
     */
    private final PersonRepository personsRepository = new PersonRepository();

    /**
     * A map of {@link StringItemsDictionary} descriptors (name of fields) for searching.
     */
    private final Map<String, StringItemsDictionary> searchableFields = new HashMap<>();

    @Value("${persons.default.file.name}")
    private String defaultPersonsFileName;

    /**
     * Initialize dictionaries with default data.
     */
    @PostConstruct
    private void initDictionaries() {
        searchableFields.put("name",       new NamesDictionary());
        searchableFields.put("secondName", new SecondNamesDictionary());
        searchableFields.put("thirdName",  new ThirdNamesDictionary());

        // Subscribe dictionaries on change events in a Person storage.
        searchableFields.forEach((fieldName, dictionary) -> personsRepository.subscribeForRepositoryChangeEvent(dictionary));

        // Load predefined default persons from local file
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            PersonObjectStorage personShortObjects =
                    objectMapper.readValue(new File(defaultPersonsFileName), PersonObjectStorage.class);

            personShortObjects.getPersons().forEach(
                    object -> personsRepository.putPerson(object.parse()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return a person from a storage with specified {@code id}.
     *
     * @param  id person ID
     * @return {@link Person} object
     */
    public Person getPerson(long id) {
        return personsRepository.getPerson(id);
    }

    /**
     * Merge an object of {@code person} which contains changes
     * with the same initial object from a storage.
     *
     * @param  person person with changes
     * @return {@link Person} object with changes applied
     */
    public Person mergePerson(Person person) {
        return personsRepository.mergePerson(person);
    }

    /**
     * Delete a {@code person} from a storage with specified {@code id}.
     *
     * @param  id person ID
     */
    public void deletePerson(long id) {
        personsRepository.deletePerson(id);
    }

    /**
     * Return all persons from a storage.
     *
     * @return a list of {@link Person} objects
     */
    public List<Person> getAllPersons() {
        return personsRepository.getAllPersons();
    }

    /**
     * Return persons from a storage by specified {@code fieldValue}.
     *
     * @param fieldName  field name to be searched
     * @param fieldValue field value to be searched
     * @param isExactEquivalence flag of necessity exact match searching
     * @return list of {@link Person} objects
     */
    public List<Person> getPersons(String fieldName, String fieldValue, Boolean isExactEquivalence) {

        // Obtain particular dictionary from a Map of registered dictionaries by a given name
        StringItemsDictionary dict = searchableFields.get(fieldName);
        if (dict == null) {
            throw new PersonServiceException(String.format("Wrong field name = [%s].", fieldName));
        }

        // Retrieve from the dictionary IDs of entries which match given parameters
        List<Long> idsWithSameNames = dict.getKeysByValueEquivalence(fieldValue, isExactEquivalence);

        // For now we have concrete IDs,
        // so it is just needed to Collect all persons with IDs equal to the retrieved ones at previous step
        Collector<Long, List<Person>, List<Person>> collector = Collector.of(
                ArrayList::new,
                (personsWithSameNames, id) -> personsWithSameNames.add(personsRepository.getPerson(id)),
                (personsWithSameNames, output) -> output = personsWithSameNames
        );
        return idsWithSameNames.stream().collect(collector);
    }
}
