package ru.pd.personsearch.dao;

import ru.pd.personsearch.model.entity.Person;

/**
 * The dictionary stores all third names of persons
 * from a storage for searching purposes.
 */
public class ThirdNamesDictionary extends StringItemsDictionary {
    @Override
    public void putItem(long id, Person item) {
        putItem(item.getThirdName(), id);
    }
}
