package ru.pd.personsearch.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Data Transfer Object for {@link Exception}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Value
@AllArgsConstructor
public class PersonServiceExceptionObject {

    @JsonProperty("requestStatus")
    private static final String STATUS = "Internal exception";

    @JsonProperty("requestMessage")
    private String message;

    private String stackTrace;

    public PersonServiceExceptionObject(Exception exception) {
        message = exception.getMessage();
        stackTrace = Arrays.stream(exception.getStackTrace())
                .map(StackTraceElement::toString)
                .collect(Collectors.joining("\n"));
    }
}
