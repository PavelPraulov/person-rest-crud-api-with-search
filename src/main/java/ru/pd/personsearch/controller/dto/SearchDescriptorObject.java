package ru.pd.personsearch.controller.dto;

import lombok.Value;

/**
 * Search parameters.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Value
public class SearchDescriptorObject {
    private String fieldName;
    private String fieldValue;
    private Boolean isExactEquivalence;
}
