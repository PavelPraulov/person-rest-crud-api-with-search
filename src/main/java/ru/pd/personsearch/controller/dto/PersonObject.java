package ru.pd.personsearch.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pd.personsearch.model.PersonBase;
import ru.pd.personsearch.model.entity.Gender;
import ru.pd.personsearch.model.entity.LegalType;
import ru.pd.personsearch.model.entity.Person;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Person Data Transfer Object.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Data
@NoArgsConstructor
public class PersonObject {
    private Long   id;
    private String name;
    private String secondName;
    private String thirdName;
    private String gender;
    private String birthday;
    private String legalType;


    public Person parse() {
        return new PersonBase(id, name, secondName, thirdName, Gender.valueOf(gender), LocalDate.parse(birthday), null,
                (legalType != null) ? LegalType.valueOf(legalType) : LegalType.getDefaultGender());
    }

    public PersonObject(Person person) {
        id   = person.getId();
        name = person.getName();
        secondName = person.getSecondName();
        thirdName  = person.getThirdName();
        gender     = person.getGender().toString();
        birthday   = (person.getBirthday() != null)
                ? person.getBirthday().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                : null;

        legalType = person.getLegalType().toString();
    }

    public static List<PersonObject> getPersonObjects(List<Person> persons) {
        List<PersonObject> objects = new ArrayList<>();
        persons.forEach(item  -> objects.add(new PersonObject(item)));
        return objects;
    }
}
