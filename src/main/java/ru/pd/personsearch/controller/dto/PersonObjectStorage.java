package ru.pd.personsearch.controller.dto;

import lombok.Value;

import java.util.List;

/**
 * Container for {@link PersonObject} .
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.3 30.06.2019
 */
@Value
public class PersonObjectStorage {
    List<PersonObject> persons;
}
