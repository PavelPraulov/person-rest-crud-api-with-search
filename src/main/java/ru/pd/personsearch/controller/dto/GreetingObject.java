package ru.pd.personsearch.controller.dto;

import lombok.Value;

/**
 * Greeting Data Transfer Object.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Value
public class GreetingObject {
    private final long id;
    private final String content;
}