package ru.pd.personsearch.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.pd.personsearch.model.search.SearchableEntityFieldInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provider of field names which support searching.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Getter
@NoArgsConstructor
public class SearchSupportedObjectList {
    private final List<String> searchSupportedFields = new ArrayList<>();
    private Integer fieldsCount;

    public SearchSupportedObjectList(Collection<SearchableEntityFieldInfo> fieldInfoList) {

        fieldInfoList.forEach(fieldInfo -> searchSupportedFields.add(fieldInfo.getEntityFieldName()));
        fieldsCount = searchSupportedFields.size();
    }
}
