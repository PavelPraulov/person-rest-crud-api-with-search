package ru.pd.personsearch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.pd.personsearch.controller.dto.GreetingObject;
import ru.pd.personsearch.controller.dto.PersonObject;
import ru.pd.personsearch.controller.dto.PersonServiceExceptionObject;
import ru.pd.personsearch.controller.dto.SearchDescriptorObject;
import ru.pd.personsearch.controller.dto.SearchSupportedObjectList;
import ru.pd.personsearch.model.exception.PersonServiceException;
import ru.pd.personsearch.processor.PersonProcessor;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

/**
 * REST Controller for Person API.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@RestController
public class PersonController {

    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private PersonProcessor processor;

    /**
     * Greeting API for testing service purposes.
     *
     * @param  name name which is used for greeting
     * @return greeting object
     */
    @RequestMapping(value = "/greeting", method=RequestMethod.GET)
    public GreetingObject greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new GreetingObject(
                counter.incrementAndGet(),
                String.format("Hello, %s!", name));
    }

    /**
     * Update person in a storage using parameter object with changes.
     *
     * @param  modifiedPerson person object with changes
     * @return person object with applied changes
     */
    @RequestMapping(
            value    = "/person",
            method   = RequestMethod.PUT,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity updatePerson(@RequestBody PersonObject modifiedPerson) {
        Function<PersonObject, PersonObject> updateFunction = processor::updatePerson;
        return handleProcessorMethodCall(updateFunction, modifiedPerson);
    }

    /**
     * Return all persons from a storage.
     *
     * @return a list of Person objects
     */
    @RequestMapping(
            value    = "/persons",
            method   = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity getPersons() {
        Function<Object, List<PersonObject>> function = parameter -> processor.getAllPersons();
        return handleProcessorMethodCall(function, null);
    }

    /**
     * Return a person with specified {@code id}.
     *
     * @param  id person ID
     * @return response entity with a status.
     */
    @RequestMapping(
            value    = "/person/{id}",
            method   = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity getPerson(@PathVariable("id") long id) {
        Function<Long, PersonObject> getFunction = personId -> processor.getPerson(personId);
        return handleProcessorMethodCall(getFunction, id);
    }

    /**
     * Delete a person with specified {@code id}.
     *
     * @param  id person ID
     * @return response entity with a status.
     */
    @RequestMapping(
            value    = "/person/{id}",
            method   =  RequestMethod.DELETE,
            produces = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity deletePerson(@PathVariable("id") long id) {
        Function<Long, Object> deleteFunction = personId -> {
            processor.deletePerson(personId);
            return null;
        };
        return handleProcessorMethodCall(deleteFunction, id);
    }

    /**
     * Return an object with field names which support
     * searching for a specified entity.
     *
     * @param  entityName name of Entity for searching in.
     * @return an object with field names which support searching
     */
    @RequestMapping(
            value    = "/persons/search/available/fields/{entityName}",
            method   = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity getSearchableFields(@PathVariable("entityName") String entityName) {
        Function<String, SearchSupportedObjectList> function =
                functionEntityName -> processor.getSearchableFields(functionEntityName);

        return handleProcessorMethodCall(function, entityName);
    }

    /**
     * A Search of a {@link PersonObject} by field value.
     *
     * @param  searchDescriptor search parameters
     * @return Person object
     */
    @RequestMapping(
            value    = "/persons/search",
            method   = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity getPersonsBySample(@RequestBody SearchDescriptorObject searchDescriptor) {
        Function<SearchDescriptorObject, List<PersonObject>> function =
                descriptor -> processor.getPersonsByFieldValue(descriptor);

        return handleProcessorMethodCall(function, searchDescriptor);

    }

    /**
     * Wrap call of a {@link PersonProcessor} method with error handling.
     *
     * @param  processorMethod method of {@link PersonProcessor}, which will be called
     * @param  methodArgument argument of the method
     * @param  <T> type of the method argument
     * @param  <R> return type of the given method
     * @return response entity with a method call result
     */
     <T, R> ResponseEntity handleProcessorMethodCall(Function<T, R> processorMethod, T methodArgument) {
        ResponseEntity responseEntity;
        try {
            responseEntity =  new ResponseEntity<R>(
                    processorMethod.apply(methodArgument),
                    HttpStatus.OK);

        } catch (PersonServiceException exception) {
            responseEntity = new ResponseEntity<>(
                    new PersonServiceExceptionObject(exception),
                    HttpStatus.BAD_REQUEST);

        } catch (Exception exception) {
        responseEntity = new ResponseEntity<>(
                new PersonServiceExceptionObject(exception),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }


        return responseEntity;
    }
}