package ru.pd.personsearch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pd.personsearch.model.entity.Gender;
import ru.pd.personsearch.model.entity.LegalType;
import ru.pd.personsearch.model.entity.Person;
import ru.pd.personsearch.model.entity.PersonDocument;

import java.time.LocalDate;
import java.util.List;

/**
 * Base implementation of {@link Person}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonBase implements Person {
    private Long id;
    private String name;
    private String secondName;
    private String thirdName;
    private Gender gender;
    private LocalDate birthday;
    private List<PersonDocument> documents;
    private LegalType legalType;
}
