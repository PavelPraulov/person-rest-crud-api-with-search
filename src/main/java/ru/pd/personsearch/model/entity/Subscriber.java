package ru.pd.personsearch.model.entity;

public interface Subscriber extends ServiceConsumer {
    @Override
    Long getId();
}
