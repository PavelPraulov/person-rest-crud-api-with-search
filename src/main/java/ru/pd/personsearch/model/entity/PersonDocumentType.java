package ru.pd.personsearch.model.entity;

public enum PersonDocumentType {
    PASSPORT,
    INTERNATIONAL_PASS,
    DRIVER_LICENSE,
    INSURANCE_POLICY,
    TAXPAYER_POLICY
}
