package ru.pd.personsearch.model.entity;

public enum LegalType {
    NATURAL,
    LEGAL_ENTITY,
    SOLE_PROPRIETORSHIP;

    static public LegalType getDefaultGender() {
        return NATURAL;
    }
}
