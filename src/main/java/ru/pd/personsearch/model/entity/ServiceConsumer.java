package ru.pd.personsearch.model.entity;

public interface ServiceConsumer extends Person {
    Account getAccount();
    Service getService();
}
