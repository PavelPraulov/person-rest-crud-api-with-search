package ru.pd.personsearch.model.entity;

public interface PersonDocument {
    PersonDocumentType getDocumentType();
    String getDocumentId();
}
