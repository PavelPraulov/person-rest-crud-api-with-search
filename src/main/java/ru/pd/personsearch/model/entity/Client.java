package ru.pd.personsearch.model.entity;

import java.util.List;

public interface Client extends ServiceConsumer {
    @Override
    Long getId();
    List<Subscriber> getSubscribers();
}
