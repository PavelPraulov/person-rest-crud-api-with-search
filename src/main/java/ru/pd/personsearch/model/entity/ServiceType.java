package ru.pd.personsearch.model.entity;

public enum ServiceType {
    FIXED_INTERNET,
    FIXED_VOICE,
    TV,
    MOBILE_INTERNET,
    MOBILE_VOICE,
    DEVICE,
    SERVICE
}
