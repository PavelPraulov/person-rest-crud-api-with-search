package ru.pd.personsearch.model.entity;

public interface Service {
    Long getId();
    ServiceType getType();
    String getName();
    String getNumber();
    Boolean isActual();
}
