package ru.pd.personsearch.model.entity;

public enum Gender {
    MAN,
    WOMAN,
    OTHER
}
