package ru.pd.personsearch.model.entity;

import ru.pd.personsearch.model.search.Searchable;

import java.time.LocalDate;
import java.util.List;

/**
 * Entity, which describes a particular human.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
public interface Person {
    Long   getId();
    void setId(Long id);

    /**
     * @return a person first name, like: "Tomas", "Heinrich", "Otto".
     */
    @Searchable("name")
    String getName();

    /**
     * @return a person last name, like: "Munz, "Ferdinand", "Von Bismarck".
     */
    @Searchable("secondName")
    String getSecondName();

    /**
     * @return a person middle name, like: "Georgievich, Theodor".
     */
    @Searchable("thirdName")
    String getThirdName();

    /**
     * @return a person gender.
     */
    Gender getGender();

    /**
     * @return a person birthday.
     */
    LocalDate getBirthday();

    /**
     * @return a supported person document list.
     */
    List<PersonDocument> getDocuments();

    /**
     * @return a person legal type.
     */
    LegalType getLegalType();
}
