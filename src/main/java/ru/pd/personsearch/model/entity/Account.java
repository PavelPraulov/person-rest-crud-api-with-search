package ru.pd.personsearch.model.entity;


public interface Account {
    Long getId();
    Long getNum();
    Boolean isActual();


}
