package ru.pd.personsearch.model.search;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * A Marker highlights entity(class) fields which can be used for searching a person.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Searchable {
    String value() default "";
}
