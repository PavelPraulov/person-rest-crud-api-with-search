package ru.pd.personsearch.model.search;

import lombok.experimental.UtilityClass;
import org.apache.commons.text.WordUtils;
import ru.pd.personsearch.model.exception.PersonServiceException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A Provider of information about {@link Searchable} entity(class) member.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@UtilityClass
public class EntitySearchMetadataProvider {
    private static final String ENTITY_PACKAGE = "ru.pd.personsearch.model.entity";

    /**
     * Provide a {@link HashMap} with containers {@link SearchableEntityFieldInfo} for
     * every annotated as {@link Searchable} member of the {@code clazz}.
     *
     * @param  clazz class to be searched
     * @return a {@link HashMap} with containers {@link SearchableEntityFieldInfo}
     */
    private static Map<String, SearchableEntityFieldInfo> getClassFieldsInfo(Class clazz) {
        Map<String, SearchableEntityFieldInfo> fieldsInfoMap = new HashMap<>();

        Arrays.stream(clazz.getDeclaredMethods())
                .filter(classField -> classField.isAnnotationPresent(Searchable.class))
                .forEach(searchableField -> fieldsInfoMap.put(
                        searchableField.getDeclaredAnnotation(Searchable.class).value(),
                        new SearchableEntityFieldInfo(
                                clazz.getSimpleName(),
                                searchableField.getDeclaredAnnotation(Searchable.class).value(),
                                clazz
                        )
                ));
        return fieldsInfoMap;
    }

    /**
     * Provide a {@link Class} object with the name = {@code entityName},
     * which located in the package = {@link EntitySearchMetadataProvider#ENTITY_PACKAGE}.
     *
     * @param  entityName a name of a class
     * @return a {@link Class} object with the name = {@code entityName}
     */
    private static Class getEntityClassRepresentation(String entityName) {
        try {
            return Class.forName(ENTITY_PACKAGE + "." + WordUtils.capitalize(entityName));
        } catch (ClassNotFoundException e) {
            throw new PersonServiceException(String.format("Entity with name = [%s] not found.", entityName));
        }
    }

    public static Map<String, SearchableEntityFieldInfo> getClassFieldsInfo(String entityName) {
        return getClassFieldsInfo(getEntityClassRepresentation(entityName));
    }
}
