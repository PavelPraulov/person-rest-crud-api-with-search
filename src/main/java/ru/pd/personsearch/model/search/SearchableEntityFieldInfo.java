package ru.pd.personsearch.model.search;

import lombok.Value;

/**
 * A Container for information about {@link Searchable} entity(class) member.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@Value
public class SearchableEntityFieldInfo {
    private String entityName;
    private String entityFieldName;
    private Class  entityRepresentClass;
}
