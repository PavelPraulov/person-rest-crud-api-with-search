package ru.pd.personsearch.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.pd.personsearch.model.entity.Person;
import ru.pd.personsearch.model.exception.PersonServiceException;

import java.util.List;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonDaoTest {

    @Autowired
    private PersonDao personDao;

    /**
     * Test {@link PersonDao#getPersons(String, String, Boolean)}
     * throws {@link PersonServiceException} on wrong {@code fieldName}.
     */
    @Test(expected = PersonServiceException.class)
    public void getPersonsThrowsPersonServiceExceptionOnWrongFieldName() {
        personDao.getPersons("WrongFieldName", anyString(), anyBoolean());
    }

    /**
     * Test {@link PersonDao#getPersons(String, String, Boolean)}
     * returns empty list of {@link Person} on a wrong given name of a person.
     */
    @Test
    public void getPersonsReturnsEmptyListOnWrongName() {
        List<Person> persons = personDao.getPersons("name", "WrongName", anyBoolean());
        Assert.assertTrue("Person list must be empty", persons.isEmpty());
    }

    /**
     * Test {@link PersonDao#getPersons(String, String, Boolean)}
     * returns instance of {@link Person} on a correct given name of a person.
     */
    @Test
    public void getPersonsReturnsPerson() {
        String expectedPersonName = "William";
        List<Person> persons = personDao.getPersons("name", expectedPersonName, anyBoolean());

        Assert.assertEquals("Person list must contain just one entry", 1, persons.size());
        Assert.assertEquals("Person list must contain Person with name 'William'",
                expectedPersonName, persons.get(0).getName());
    }
}