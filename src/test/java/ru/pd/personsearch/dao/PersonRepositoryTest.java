package ru.pd.personsearch.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import ru.pd.personsearch.controller.dto.PersonObjectStorage;
import ru.pd.personsearch.model.PersonBase;
import ru.pd.personsearch.model.entity.Person;
import ru.pd.personsearch.model.exception.PersonServiceException;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Random;

/**
 * A set of tests for {@link PersonRepository}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.2 13.06.2019
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PersonRepositoryTest {

    private final PersonRepository personsRepository = new PersonRepository();

    @Value("${persons.default.file.name}")
    private String defaultPersonsFileName;

    /**
     * Test person id generation on predefined data in case
     * then the generation are called first time.
     *
     * @throws IOException in case of an error during loading of JSON file
     */
    @Test
    public void generateIdFirstTimeOnPredefinedData() throws IOException {

        // Load predefined default persons from local file
        loadPredefinedDataFromJson();
        Assert.assertEquals("There must be loaded 3 entries.", 3, personsRepository.getAllPersons().size());
        Assert.assertEquals("The first time called sequence must return a 4.", 4, personsRepository.generateId());
    }

    /**
     * Test person id generation on predefined data in case
     * then the generation are called many time.
     *
     * @throws IOException in case of an error during loading of JSON file
     */
    @Test
    public void generateIdManyTimeOnPredefinedData() throws IOException {

        // Load predefined default persons from local file
        loadPredefinedDataFromJson();

        Assert.assertEquals("There must be loaded 3 entries.", 3, personsRepository.getAllPersons().size());
        Random r = new Random();
        int generateTimes = r.nextInt(10);
        long actualResult = 0L;
        for (int i = 0; i <= generateTimes; i++) {
            actualResult = personsRepository.generateId();
        }

        Assert.assertEquals("The sequence must return expected value.", 4 + generateTimes, actualResult);
    }

    /**
     * Test private method {@code checkId()} via {@link PersonRepository#getPerson(long)}.
     * It must throw an exception {@link PersonServiceException} on negative input ID.
     *
     * @throws PersonServiceException on negative input ID
     */
    @Test(expected = PersonServiceException.class)
    public void checkIdThrowsPersonServiceExceptionOnNegativeId() {
        personsRepository.getPerson(-1L);
    }

    /**
     * Test private method {@code checkId()} via {@link PersonRepository#getPerson(long)}.
     * It must throw an exception {@link PersonServiceException} on correct input ID,
     * but empty a person Map.
     *
     * @throws PersonServiceException on negative input ID
     */
    @Test(expected = PersonServiceException.class)
    public void checkIdThrowsPersonServiceExceptionOnEmptyPersonMap() {
        personsRepository.getPerson(1L);
    }

    /**
     * Test {@link PersonRepository#getPerson(long)}.
     * It must throw an exception {@link PersonServiceException} on correct input ID,
     * but empty a person Map.
     *
     * @throws PersonServiceException on negative input ID
     */
    @Test(expected = PersonServiceException.class)
    public void getPersonThrowsPersonServiceExceptionOnEmptyPersonMap() {
        personsRepository.getPerson(1L);
    }

    /**
     * Test {@link PersonRepository#getPerson(long)} on correct ID of entry which exists in a person map.
     */
    @Test
    public void getPersonReturnPersonOnCorrectId() {
        Person expectedPerson = new PersonBase(1L, null, null, null, null, null, null, null);
        ReflectionTestUtils.setField(personsRepository, "persons", Collections.singletonMap(1L, expectedPerson));

        Person actualPerson = personsRepository.getPerson(1L);
        Assert.assertSame("Actual object must be the same as expected one", expectedPerson, actualPerson);
    }


    /**
     * Load predefined data from JSON local file.
     *
     * @throws IOException in case of an error during loading of JSON file
     */
    private void loadPredefinedDataFromJson() throws IOException {
        PersonObjectStorage personShortObjects =
                new ObjectMapper().readValue(new File(defaultPersonsFileName), PersonObjectStorage.class);

        personShortObjects.getPersons().forEach(
                object -> personsRepository.putPerson(object.parse()));
    }

}