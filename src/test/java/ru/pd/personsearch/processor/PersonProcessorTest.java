package ru.pd.personsearch.processor;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import ru.pd.personsearch.dao.PersonDao;
import ru.pd.personsearch.model.PersonBase;
import ru.pd.personsearch.model.entity.Gender;
import ru.pd.personsearch.model.entity.LegalType;
import ru.pd.personsearch.model.entity.Person;
import ru.pd.personsearch.model.exception.PersonServiceException;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * A set of tests for {@link PersonProcessor}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.2 13.06.2019
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PersonProcessorTest {

    @Autowired
    private PersonProcessor processor;

    /**
     * Test {@link PersonProcessor#getPerson(long)}
     * returns expected Person
     * on a particular correct given ID.
     */
    @Test
    public void getPersonReturnEntryOnCorrectId() {
        Person expectedPerson = getDummyPerson();

        Assert.assertEquals("Id of the persons must equal",
                expectedPerson.getId(), processor.getPerson(1L).getId());

    }

    /**
     * Test {@link PersonProcessor#getPerson(long)}
     * throw {@link PersonServiceException}
     * on a given not existing in repository ID.
     */
    @Test(expected = PersonServiceException.class)
    public void getPersonReturnNullOnWrongId() {
        processor.getPerson(0L);

    }

    /**
     * Test {@link PersonProcessor#getPerson(long)}
     * does a call of {@link PersonDao#getPerson(long)}.
     */
    @Test
    public void getPersonCallsDaoMethod() {
        PersonProcessor processorMock =  mock(PersonProcessor.class);
        PersonDao daoMock = mock(PersonDao.class);
        when(daoMock.getPerson(anyLong())).thenReturn(getDummyPerson());
        ReflectionTestUtils.setField(processorMock, "personDAO", daoMock);
        when(processorMock.getPerson(anyLong())).thenCallRealMethod();

        processorMock.getPerson(anyLong());
        Mockito.verify(daoMock, times(1)).getPerson(anyLong());

    }

    /**
     * Make a basic instance of {@link Person}
     *
     * @return instance of {@link Person}
     */
    private static Person getDummyPerson() {
        return new PersonBase(1L, null, null, null, Gender.MAN, null, null, LegalType.NATURAL);
    }
}