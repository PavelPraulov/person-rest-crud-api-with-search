package ru.pd.personsearch.model.search;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import ru.pd.personsearch.model.exception.PersonServiceException;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * A set of tests for {@link EntitySearchMetadataProvider}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.1 13.06.2018
 */
@RunWith(SpringRunner.class)
public class EntitySearchMetadataProviderTest {

    /**
     * Test occupancy of a result map
     * from a method {@link EntitySearchMetadataProvider#getClassFieldsInfo(String)}
     */
    @Test
    public void getClassFieldsInfoMapContainsThreeEntries() {
        String entityName = "Person";
        Map<String, SearchableEntityFieldInfo> fieldsInfoMap =
                EntitySearchMetadataProvider.getClassFieldsInfo(entityName);
        assertNotNull("The map must not be null", fieldsInfoMap);
        assertEquals("The map must contain 3 entries", 3, fieldsInfoMap.size());
    }

    /**
     * Test {@link EntitySearchMetadataProvider#getClassFieldsInfo(String)
     * throws {@link PersonServiceException} on wrong {@code entityName}
     */
    @Test(expected = PersonServiceException.class)
    public void getClassFieldsInfoThrowsClassNotFoundExceptionOnWrongEntityName() {
        String entityName = "NotAPerson";
        Map<String, SearchableEntityFieldInfo> fieldsInfoMap =
                EntitySearchMetadataProvider.getClassFieldsInfo(entityName);
    }

}