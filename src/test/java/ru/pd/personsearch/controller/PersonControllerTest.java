package ru.pd.personsearch.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.ReflectionUtils;
import ru.pd.personsearch.controller.dto.GreetingObject;
import ru.pd.personsearch.controller.dto.PersonObject;
import ru.pd.personsearch.controller.dto.SearchDescriptorObject;
import ru.pd.personsearch.model.PersonBase;
import ru.pd.personsearch.model.entity.Gender;
import ru.pd.personsearch.model.entity.LegalType;
import ru.pd.personsearch.model.exception.PersonServiceException;
import ru.pd.personsearch.processor.PersonProcessor;

import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A set of tests for {@link PersonController}.
 *
 * @author  Pavel Praulov
 * @version 0.0.2
 * @since   0.0.2 13.06.2019
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PersonControllerTest {

    @Autowired
    PersonController controller;


    /**
     * Test increasing a {@link PersonController} counter
     * within a call of {@link PersonController#greeting(String)} function.
     */
    @Test
    public void greetingCheckCounter() {
        PersonController localController = new PersonController();
        GreetingObject greetingObjectOne = localController.greeting(null);
        assertEquals("A greeting id from result object must equal expected one",
                1L, greetingObjectOne.getId());

        GreetingObject greetingObjectTwo = localController.greeting(null);

        assertEquals("A greeting id from result object must equal expected one",
                2L, greetingObjectTwo.getId());
    }

    /**
     * Test getting an expected not null name
     * from {@link PersonController#greeting(String)} function.
     */
    @Test
    public void greetingCheckWithNotNullName() {
        String expectedName = "NameOne";
        GreetingObject greetingObject = controller.greeting(expectedName);
        assertEquals(
                "A greeting phrase from result object must equal expected one",
                String.format("Hello, %s!", expectedName),
                greetingObject.getContent());
    }

    /**
     * Test an order of the calls within
     * {@link PersonController#updatePerson(PersonObject)}
     */
    @Test
    public void updatePersonCheckOrder() {
        PersonProcessor processorMock = mock(PersonProcessor.class);
        PersonController controllerMock = getPersonControllerMock(processorMock);

        when(processorMock.updatePerson(any())).thenReturn(null);
        when(controllerMock.updatePerson(any())).thenCallRealMethod();
        when(controllerMock.handleProcessorMethodCall(any(), any())).thenCallRealMethod();

        InOrder inOrder = inOrder(processorMock, controllerMock);

        controllerMock.updatePerson(any());

        inOrder.verify(controllerMock).handleProcessorMethodCall(any(), any());
        inOrder.verify(processorMock).updatePerson(any());
    }

    /**
     * Test an order of the calls within
     * {@link PersonController#getPersonsBySample(SearchDescriptorObject)}
     */
    @Test
    public void getPersonsCheckOrder() {
        PersonProcessor processorMock = mock(PersonProcessor.class);
        PersonController controllerMock = getPersonControllerMock(processorMock);

        when(processorMock.getAllPersons()).thenReturn(null);
        when(controllerMock.getPersons()).thenCallRealMethod();
        when(controllerMock.handleProcessorMethodCall(any(), any())).thenCallRealMethod();

        InOrder inOrder = inOrder(processorMock, controllerMock);

        controllerMock.getPersons();

        inOrder.verify(controllerMock).handleProcessorMethodCall(any(), any());
        inOrder.verify(processorMock).getAllPersons();
    }

    /**
     * Test an order of the calls within
     * {@link PersonController#getPerson(long)}
     */
    @Test
    public void getPersonCheckOrder() {
        PersonProcessor  processorMock  = mock(PersonProcessor.class);
        PersonController controllerMock = getPersonControllerMock(processorMock);

        when(processorMock.getPerson(anyLong())).thenReturn(null);
        when(controllerMock.getPerson(anyLong())).thenCallRealMethod();
        when(controllerMock.handleProcessorMethodCall(any(), anyLong())).thenCallRealMethod();

        InOrder inOrder = inOrder(processorMock, controllerMock);

        controllerMock.getPerson(anyLong());

        inOrder.verify(controllerMock).handleProcessorMethodCall(any(), anyLong());
        inOrder.verify(processorMock).getPerson(anyLong());
    }

    /**
     * Test an order of the calls within
     * {@link PersonController#deletePerson(long)}
     */
    @Test
    public void deletePersonCheckOrder() {
        PersonProcessor processorMock = mock(PersonProcessor.class);
        PersonController controllerMock = getPersonControllerMock(processorMock);

        doNothing().when(processorMock).deletePerson(anyLong());
        when(controllerMock.handleProcessorMethodCall(any(), anyLong())).thenCallRealMethod();
        when(controllerMock.deletePerson(anyLong())).thenCallRealMethod();

        InOrder inOrder = inOrder(processorMock, controllerMock);

        controllerMock.deletePerson(anyLong());

        inOrder.verify(controllerMock).handleProcessorMethodCall(any(), anyLong());
        inOrder.verify(processorMock).deletePerson(anyLong());
    }

    /**
     * Test an order of the calls within
     * {@link PersonController#getPersonsBySample(SearchDescriptorObject)}
     */
    @Test
    public void getSearchableFieldsCheckOrder() {
        PersonProcessor processorMock = mock(PersonProcessor.class);
        PersonController controllerMock = getPersonControllerMock(processorMock);

        when(processorMock.getSearchableFields(any())).thenReturn(null);
        when(controllerMock.handleProcessorMethodCall(any(), any())).thenCallRealMethod();
        when(controllerMock.getSearchableFields(any())).thenCallRealMethod();

        InOrder inOrder = inOrder(processorMock, controllerMock);

        controllerMock.getSearchableFields(any());

        inOrder.verify(controllerMock).handleProcessorMethodCall(any(), any());
        inOrder.verify(processorMock).getSearchableFields(any());
    }

    /**
     * Test an order of the calls within
     * {@link PersonController#getPersonsBySample(SearchDescriptorObject)}
     */
    @Test
    public void getPersonsBySampleCheckOrder() {
        PersonProcessor processorMock = mock(PersonProcessor.class);
        PersonController controllerMock = mock(PersonController.class);
        ReflectionTestUtils.setField(controllerMock, "processor", processorMock);

        when(processorMock.getPersonsByFieldValue(any())).thenReturn(null);
        when(controllerMock.handleProcessorMethodCall(any(), any())).thenCallRealMethod();
        when(controllerMock.getPersonsBySample(any())).thenCallRealMethod();

        InOrder inOrder = inOrder(processorMock, controllerMock);

        controllerMock.getPersonsBySample(any());

        inOrder.verify(controllerMock).handleProcessorMethodCall(any(), any());
        inOrder.verify(processorMock).getPersonsByFieldValue(any());
    }


    /**
     * Test a private method PersonController.handleProcessorMethodCall(Function, Object)
     * for handling thrown {@link PersonServiceException}.
     *
     * @throws NoSuchMethodException when {@code controller} doesn't contain
     * a method {@code handleProcessorMethodCall}
     */
    @Test
    public void handleProcessorMethodCallReturnBadRequestOnPersonServiceException() throws NoSuchMethodException {
        PersonProcessor processor = mock(PersonProcessor.class);
        doThrow(new PersonServiceException("A declared exception")).when(processor).updatePerson(any());

        Function<PersonObject, PersonObject> function = processor::updatePerson;

        ResponseEntity entity = controller.handleProcessorMethodCall(
                function,
                getPersonObject());

        assertEquals("Response entity must contain Bad Request status code",
                HttpStatus.BAD_REQUEST, entity.getStatusCode());

    }

    /**
     * Test a private method PersonController.handleProcessorMethodCall(Function, Object)
     * for handling thrown any exception derived from {@link Exception}.
     *
     * @throws NoSuchMethodException when {@code controller} doesn't contain
     * a method {@code handleProcessorMethodCall}
     */
    @Test
    public void handleProcessorMethodCallReturnInternalErrorOnException() throws NoSuchMethodException {
        PersonProcessor processor = mock(PersonProcessor.class);
        doThrow(new RuntimeException("A declared exception")).when(processor).updatePerson(any());

        Function<PersonObject, PersonObject> throwExceptionFunction = processor::updatePerson;

        ResponseEntity entity = controller.handleProcessorMethodCall(
                throwExceptionFunction,
                getPersonObject());

        assertEquals("Response entity must contain Server Error status code",
                HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());

    }

    /**
     * Test a private method PersonController.handleProcessorMethodCall(Function, Object)
     * for handling passed {@link PersonObject}.
     *
     * @throws NoSuchMethodException when {@code controller} doesn't contain
     * a method {@code handleProcessorMethodCall}
     */
    @Test
    public void handleProcessorMethodCallReturnOkOnPersonObject() throws NoSuchMethodException {
        PersonProcessor processor = mock(PersonProcessor.class);
        doReturn(getPersonObject()).when(processor).updatePerson(any());

        Function<PersonObject, PersonObject> throwExceptionFunction = processor::updatePerson;

        ResponseEntity entity = controller.handleProcessorMethodCall(
                throwExceptionFunction,
                getPersonObject());

        assertEquals("Response entity must contain OK status code",
                HttpStatus.OK, entity.getStatusCode());

    }

    private PersonController getPersonControllerMock(PersonProcessor containedProcessor) {
        PersonController controllerMock = mock(PersonController.class);
        ReflectionTestUtils.setField(controllerMock, "processor", containedProcessor);
        return controllerMock;
    }


    /**
     * Provide an accessible method {@code PersonController#handleProcessorMethodCall(Function, Object)}
     *
     * @throws NoSuchMethodException when {@code controller} doesn't contain
     * a method {@code handleProcessorMethodCall}
     */
    private Method getAccessibleHandleProcessorMethodCall() throws NoSuchMethodException {
        Method method = controller.getClass().getDeclaredMethod(
                "handleProcessorMethodCall", Function.class, Object.class);
        ReflectionUtils.makeAccessible(method);
        return method;
    }

    /**
     * Provide a default filled instance of {@link PersonObject}.
     * @return instance of {@link PersonObject}
     */
    private static PersonObject getPersonObject() {
        return new PersonObject(new PersonBase(0L, "name", "secondName", "thirdName",
                Gender.OTHER, LocalDate.now(), null, LegalType.NATURAL));
    }

}